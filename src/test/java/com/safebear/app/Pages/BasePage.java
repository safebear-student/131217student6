package com.safebear.app.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 14/12/2017.
 */
public class BasePage {

    WebDriver driver;

    @FindBy(linkText = "Logout")
    WebElement logoutLink;


    public static void waitForSomeTime() {
        try {

            Thread.sleep(1000);

        } catch (InterruptedException e) {

            e.printStackTrace();

        }
    }

    //check for logout linl
    //method to logout


    public boolean checkForLogoutLink() {

        return logoutLink.isDisplayed();
    }

    public void logout() {
        logoutLink.click();

    }

}
