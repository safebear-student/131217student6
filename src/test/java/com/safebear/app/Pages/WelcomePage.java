package com.safebear.app.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class WelcomePage extends BasePage {

    WebDriver driver;

    @FindBy(linkText = "Login")
    WebElement loginLink;


    //elements for logout page



    public WelcomePage(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public boolean checkCorrectPage(){

       return driver.getTitle().startsWith("Welcome");

    }

    public boolean clickOnLogin(LoginPage loginPage) {
        loginLink.click();
        return loginPage.checkCorrectPage();
    }

}
