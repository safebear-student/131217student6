package com.safebear.app;

import com.safebear.app.Pages.LoginPage;
import com.safebear.app.Pages.WelcomePage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){

        //welcomePage clickLogin();

        assertTrue(welcomePage.checkCorrectPage());

        assertTrue(welcomePage.clickOnLogin(loginPage));

        assertTrue(loginPage.login(this.userPage, "testuser","testing"));


    }

}
