package com.safebear.app;

import com.safebear.app.Pages.LoginPage;
import com.safebear.app.Pages.UserPage;
import com.safebear.app.Pages.WelcomePage;
import com.safebear.app.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 14/12/2017.
 */
public class StepDefs {

    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp(){
        utility = new Utils();
        this.driver = utility.getDriver();

        welcomePage = new WelcomePage(driver);
        driver.get(utility.getUrl());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void  tearDown(){

        driver.close();
    }


    @Given("^that I have a user (.+) with (.+) permissions and password (.+)$")
    public void create_user(String username, String permission, String password) throws Throwable {

    }

    @When("^I login with (.+) and (.+) testing$")
    public void user_logs_in(String uname, String pass) throws Throwable {

        welcomePage.clickOnLogin(loginPage);
        loginPage.login(userPage,"testuser","testing")

    }

    @Then("^I can see my account$")
    public void i_can_see_my_account() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


}