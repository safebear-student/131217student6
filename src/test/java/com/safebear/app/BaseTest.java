package com.safebear.app;

import com.safebear.app.Pages.LoginPage;
import com.safebear.app.Pages.UserPage;
import com.safebear.app.Pages.WelcomePage;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 13/12/2017.
 */
public class BaseTest {

    WebDriver driver;
    Utils utility;

    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage  userPage;


    @Before
    public void setUp(){

        utility = new Utils();
        this.driver = utility.getDriver();

        welcomePage = new WelcomePage(this.driver);
        loginPage = new LoginPage(this.driver);
        userPage = new UserPage(this.driver);



        driver.get(utility.getUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void tearDown(){

        if(userPage.checkForLogoutLink()){
            userPage.logout();
        }
        driver.close();

        //TimeUnit.SECONDS.sleep(timeout);

    }
}
